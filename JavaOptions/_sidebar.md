- 运行命令

  - [launches](launches.md)

- 选项命令

  - [standardOptions](standardOptions.md)
  - [nonStandardOptions](nonStandardOptions.md)
  - [advancedRuntimeOptions](advancedRuntimeOptions.md)
  - [advancedJITCompilerOptions](advancedJITCompilerOptions.md)
  - [advancedServiceabilityOptions](advancedServiceabilityOptions.md)
  - [advancedGarbageCollectionOptions](advancedGarbageCollectionOptions.md)