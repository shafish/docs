?> 之前说了，standardOption的命令是可以被所有的Java虚拟机运行的，那具体的命令都有哪些呢？？来，一共20条，请注意查收：

!> 这一章的命令都是以`-`开头的！为了方便书写，下面省略了。但用的时候注意在命令前要加上`-`。

## 1. agentlib:
使用`-agentlib:`可以加载本地的库文件，比如`jdk1.8.0_192\jre\bin\hprof.dll` heap profiling tool堆分析工具。
```bash
// 每20s就输出一些简单的cpu信息，而且指定的栈深度为3个栈帧。
java -agentlib:hprof=cpu=samples,interval=20,depth=3 xxx.java
```
- 如果提示找不到hprof，也可以直接用绝对路径指定：`java -agentlib:xx\jdk1.8.0_192\jre\bin\hprof=cpu=samples,interval=20,depth=3 xxx.java`

更多详细信息参考：http://docs.oracle.com/javase/8/docs/api/java/lang/instrument/package-summary.html & http://docs.oracle.com/javase/8/docs/platform/jvmti/jvmti.html#starting

## 2. client
`-client`可以指定使用client版的vm，你直接在命令行输入`java --version`可以看到最后一行默认为`server`版的hotspot！如果是64位的jdk，该`-client`指令已经无效了。

## 3. server
`-server`可以指定使用server版虚拟机，64为jdk默认使用。

## 4. Dproperty=value
可以设置一个`系统`的`属性`和`属性值`，对应上面命令的property和value，也就是说只要以`-D`开头就行（Dxx=yy，别看错了）。
```java
public class GetSystemProp {
    public static void main(String[] args) {
        String prop1 = System.getProperty("prop1", "defaultvalue");
        String prop2 = System.getProperty("prop2", "defaultvalue");
        System.out.println("prop1 = " + prop1);
        System.out.println("prop2 = " + prop2);
    }
}
```
```bash
🐟: java -Dprop1="参数1 就是我" GetSystemProp.java
prop1 = 参数1 就是我
prop2 = defaultvalue
```

## 5. enableassertions
也可以直接用`-ea`简写命令表示它。功能：指定特定包/类开启断言。
- 什么是断言`assert`，在Java中可以通过断言判断true/false，如果true就会继续执行流程。
    - 在jdk4中就被引入了，因为`assert`是一个关键字，不需要引入任何包，为了避免与以后的代码有命名冲突，所以jvm默认就关闭断言功能。
    - 这是一个几乎被开发者遗忘掉的Java关键字。

!> 所以`-ea`就是开启断言的命令。

```java
public class Accert {
    public static void main(String[] args) {
        // 使用assert 判断args数组大于0是否为true，如果为false就输出提示，如果true就继续执行args[0]
        assert (args.length > 0): " assert context is null";
        System.out.println(args[0]);
    }
}
```
```bash
🐟: java -ea Accert.java
Exception in thread "main" java.lang.AssertionError:  assert context is null
        at Accert.main(Accert.java:3)
🐟: java Accert.java
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 0 out of bounds for length 0
        at Accert.main(Accert.java:4)
```
```bash
🐟: java -ea Accert.java fisha
fisha
🐟: java Accert.java fisha
fisha
```

## 6. enablesystemassertions
也可以直接用`-esa`简写命令表示它。功能：全局开启断言，所有classpath下的类都启用断言assert关键字。

## 7. disableassertions
也可以直接用`-da`简写命令表示它。功能：指定某个包/类禁用断言。

## 8. disablesystemassertions
也可以直接用`-dsa`简写命令表示它。功能：禁用全局断言。这个命令也是jvm默认执行的。

## 9. help
也可以直接用`-?`简写命令表示它。功能：可以看看`Java`命令怎么用的。

## 10. jar
`-jar jarName`，运行这个打包成jar包形式的程序。

更多详细信息参考：http://docs.oracle.com/javase/8/docs/technotes/guides/jar/index.html & http://docs.oracle.com/javase/tutorial/deployment/jar/index.html

## 11. javaagent:
加载用`Java语言`编写的库文件。跟 [agentlib](#_1-agentlib) 只是语言上的区别。

更多详细信息参考：http://docs.oracle.com/javase/8/docs/api/java/lang/instrument/package-summary.html

## 12. jre-restrict-search
本地机器安装有多个jre时可以使用该命令进行切换，在后续的虚拟机中已经不推荐使用了

## 13. no-jre-restrict-search
include/exclude user private JREs in the version search

## 14. showversion
`java -showversion xxx`运行xxxJava程序时，顺便在开头输出一下Java版本信息。

## 15. version
输出版本信息，然后退出。

## 16. version:release
指定程序运行的Java版本（有需要再研究）。

## 17. splash:
`-splash:xx`启动程序时设置一个资源展示（开屏动画），资源可以是png、jpg、gif等（视频无效、远程资源也无效）。
```
java -splash:load.jpg GetSystemProp.java
```

## 18. verbose:class
展示运行程序时加载的每个类。那么问题来了，你知道加载一个程序时jvm都是加载哪些类呢？？

## 19. verbose:gc
输出运行程序时发生的gc事件。

## 20. verbose:jni
展示调用jni的接口相关信息。（还没用过）

