?> 为`HotSpot`虚拟机专门订制的命令。来，一共25条，请注意查收：

!> 这一章的命令都是以`-X`开头，为了方便，我又省略了`-`，亲注意。留坑：`18. Xmxsize`

## 1. X
显示所有与`-X`相关的命令说明

## 2. Xbatch
禁用后台编译，jvm默认会以后台任务的形式编译解释执行方法，禁用后台编译，编译会以前台任务的形式编译，跟`-XX:-BackgroundCompilation`功能一样，该项主要用于调试

## 3. Xbootclasspath:
指定启动类路径/资源。比如说修改或者重写jdk中的某个类：ext/*jar中某个类等。但是强烈禁止重写rt.jar包中的类！！

## 4. Xbootclasspath/a:
指定某个自定义启动类append到jdk默认的bootstrap classPath中

## 5. Xbootclasspath/p:
指定一个自定义的启动类放到默认bootstrap classPath前。

## 6. Xcheck:jni
在用jni接口调用其他语言时，先对传递给jni接口的参数和运行环境等做检查，如果出现无效的参数，JVM会中止该jni执行。

## 7. Xcomp
第一次调用某个方法时，强制编译。client版的vm会进行1000次该方法的解释执行，以达到收集足够的信息来高效编译；而server版的vm则会进行10000次。
而使用`-Xcomp`命令，则说明调用方法时 就不进行解释执行了，直接编译执行以加快执行速度。（真的快吗？？）

也可以使用`-XX:CompileThreshold`来指定编译之前的解释执行次数。

## 8. Xdebug
Does nothing.就是一占位符

## 9. Xdiag
输出诊断信息（建议实际操作感受一下）

## 10. Xfuture
启用严格的类文件格式检查，类似ES的`"use strict";`

## 11. Xint
指定程序从开始到结束都是解释执行，不使用编译执行（jit）。

## 12. Xinternalversion
`-version`命令的升级版，详细输出jvm的版本等信息。

## 13. Xloggc:filename
可以在`:`后面设置日志文件，把gc事件发生信息写入到该文件中。这个命令是包括`-verbose:gc`功能的。默认最大容量为：`240 M`。

## 14. Xmaxjitcodesize=size
指定jit编译的最大代码缓存量，也就是指定jit编译类的总容量。跟下一章的`-XX:ReservedCodeCacheSize`命令是一样的功能。

## 15. Xmixed
除了热点代码被直接编译成本地机器码外，其他代码都解释执行。

## 16. Xmnsize
设置堆中`年轻代`的初始容量size值和最大容量size值。
- 什么叫`初始和最大`？？是两个东西还是一个东西？
- `-Xmn`命令指的是把年轻代的初始容量和最大容量都设置为`一样大小`。
- 容量单位：k/K、m/M、g/G，size代表具体的值，并不是命令。

!> 因为年轻代是用来存放新建对象的区域，gc会被频繁执行，所以需要设置一个合理的大小，官方建议是设置年轻代的容量为`堆`容量的`1/2~1/4`之间！！

例子：
```
// 设置年轻代初始和最大容量为256M
-Xmn256m
-Xmn262144k
-Xmn268435456
```

- 如果需要分别指定初始容量和最大容量可以使用以下命令：
    - `-XX:NewSize=size`：初始容量
    - `-XX:MaxNewSize=size`：最大容量

## 17. Xmssize
跟上面命令类似，这里的`size`也是指具体的值。`-Xms`该命令是设置堆的初始容量，值必须是1024的倍数且>1m，k/K、m/M、g/G。

例子：
```
// 设置堆初始容量为6M
-Xms6291456
-Xms6144k
-Xms6m
```

## 18. Xmxsize
指定`内存分配池`的最大值，（我之前一直以为是设置堆的最高容量）。mark了
- 内存分配池（memory allocation pool），JVM能够使用的最大内存容量，因为内存占用最高的就是堆，所以可以粗略地理解为：设置堆的最大容量。
- 但单纯理解为堆最高容量是不正确的，因为Java方法（元空间）、栈、本地方法栈等等都需要占用一定的内存容量（只是相对于堆而言比例较小）。
- 常见的`OOM（java.lang.OutOfMemoryError）`错误就与该参数的设置相关。

慢着！！ 在末尾突然看到 `The -Xmx option is equivalent to -XX:MaxHeapSize.` 好像隐约记得`Java内存模型`好像是一个抽象层的概念，并不真实存在，emmmm学艺不精，先留个坑！！

?> https://stackoverflow.com/questions/14763079/what-are-the-xms-and-xmx-parameters-when-starting-jvm

## 19. Xnoclassgc
禁用类对象的垃圾回收，能够缩短GC时间，减少程序运行时中断。

也就说发生gc时不会回收类对象，如果不注意，很有可能发生oom。

## 20. Xprof
分析运行的程序并输出相关信息，在程序中很实用，但不应该在生产环境中使用。

## 21. Xrs
减少JVM对本地操作系统信号量的使用。相当于减少调用系统线程，比如说jvm可以通过`监听`数据库连接关闭做出`停止`程序的操作。
但是如果减少了对系统的调用，就失去了对控制台的监听，我们通过ctrl+c、ctrl+x等中断操作会无法停止程序。

这个命令建议结合文档看。

## 22. Xshare:mode
设置类数据共享的模式，也就是`mode`，mode一共有四个可选：
- auto：如果可以使用类数据共享就使用类数据共享，32位client版vm默认为该模式。
- on：强制使用类数据共享，如果没有使用则直接报错并退出程序。
- off：不使用类数据共享，在32位的server版vm中默认为这种模式，64位的vm附议。
- dump：手动设置类数据共享。

## 23. XshowSettings:category
设置vm的属性设置，`category`包括`VM settings``Property settings``Locale settings`等。默认输出全部的setting。这三个也就是可选的：
- all：默认输出全部
- vm：输出vmsetting
- property：输出参数配置相关setting
- locale：本地化相关setting

## 24. Xsssize
设置线程栈容量。k/K、m/M、g/G。
```
// 设置线程栈容量为1M大小
-Xss1m
-Xss1024k
-Xss1048576
```

## 25. Xverify:mode
设置class字节码校验器的mode（模式），用于确保类文件的格式符合规范。不建议程序关闭该功能。同样也是可以选择选项，有3个：
- remote：校验所有没被bootstrap启动类加载的类，如果没有显式使用`-Xverify`命令，则会默认调用此模式。
- all：校验所有的字节码文件。
- none：禁用字节码校验（该选项以被禁用，滑稽）。
