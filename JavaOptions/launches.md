## 一、launches a application
?> 运行Java程序的命令

```bash
java [options] classname [args]
java [options] -jar filename [args]
javaw [options] classname [args]
javaw [options] -jar filename [args]
```

## 二、Description
?> `Java`命令启动项目是通过启动了`JRE`（基本运行环境），加载了相关的类并调用了程序的`main`入口方法，或者调用了继承`javafx.application.Application`的类。后者与`JavaFX Application`相关。

- 1. Java&Javaw：
Java命令可以启动程序，并在当前命令行一直运行，可以输出包括项目log、报错等信息；
Javaw命令则以类似后台运行的方式启动程序，在当前命令行中没有任何信息输出。

- 2. options：
调整JVM相关的命令

- 3. args：
如果在命令行中设置args，则可以把该值传递给Java程序的main方法。

- `main`方法的基本结构：程序的main方法必须声明为public static，没有任何返回值而且必须提供一个String类型的参数数组。JRE加载的类只在当前机器配置的`classpath`环境中，bootstrap path（jre\lib\*.jar）->extensions path（jre\lib\ext\*.jar）->application path（程序通过maven/gradle导入的jar包）。
```java
public static void main(String args[]){...}
```

- 运行`JavaFX`程序，首先创建了一个`Application`类实例，调用了其`init()`方法，然后才调用`start(javafx.stage.Stage)`方法启动程序。

- `-jar`命令指定的参数值`filename`，是一个包含了程序运行所需类文件和配置文件的`jar`包，如果本地机器配置了`JRE`，双击即可运行该`jar`程序。

## 三、Options
?> `java [options] classname [args]`中的`Options`，在这节只做基本的介绍，详细命令操作看下一章。

!> 根据不同的功能定位可以大致分为以下`3`类：`standard`+`non-standard`+`advanced`，如下面1、2、3那样。

!> 如果仅仅是命令本身的话只分两种：`argument options`+`Boolean options`，也就是有参和无参的区别，如下面4、5那样。

- 1. standard Options：
命令以`-`开头。标准对吧，说明该选项命令能被所有Java 虚拟机所支持！！命令主要是关于：检查JRE版本、设置claspath、程序信息详细输出等等。

- 2. non-Standard Options：
命令以`-X`开头。该选项对`HotSpot`虚拟机无缝支持（其他虚拟机看情况），具体有什么用，请看下一章介绍。

- 3. advanced：
命令以`-XX`开头。高级开发者命令，对吧，不熟练的情况下就不要在生产环境上使用。命令关于调整`HotSpot`虚拟机的内存空间的，通常也需要根据不同的系统和权限来配合调整。同样是对`HotSpot`无缝支持的（其他虚拟机看情况）。
    - advanced Runtime Options
    - advanced JIT Compiler Options
    - advanced Serviceability Options
    - advanced Garbage Collection Options

- 4. argument options：
参数可以`空格`、`:`、`=`或直接跟参数的形式标明。
    - 比如需要设置内存大小：`-Xmx10240m`就可以直接把参数`1024m`写在后面，而且单位是可以调整的：`8g=8192m=8388608k=8589934592`

- 5. boolean options：
命令以`+/-`标识。布尔类型的命令，就像`开关`一样，要不是开，要不就是关。在这里是指开启或关闭某个功能。
    - 比如：开启`-XX:+OptionName`；关闭`-XX:-OptionName`
