# JavaOptDoc-Note

> JavaSE 8 Options Document. Java程序运行命令记录文档。

学了那么久Java，你是否还记得第一个启动Java程序的命令是啥，我们常说的jvm调优参数又是怎么传给Java虚拟机的呢？

来，跟着我一起学习（翻译）这篇[Java8 options document](https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html)文档吧。😊

![](_imgs/jdk8.png ":no-zoom")